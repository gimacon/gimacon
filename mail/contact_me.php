<?php
$name = $_POST['name'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$message = $_POST['message'];

$name = preg_replace('/[^ \w]+/', '', $name);
$phone = preg_replace('/[^\d-]+/', '', $phone);
$message = str_replace(array( '<', '>' ), '', $message);

$subject = 'Kontaktaufnahme: '.$name;
$headers = 'From: Gimacon'."\r\n".'X-Mailer: PHP/'.phpversion();

$to = "info@gimacon.ch";
$body = "Sie haben eine Nachricht von Ihrem Kontaktformular auf gimacon.ch erhalten:\nName: ".$name."\nEmail: ".$email."\nTelefon: ".$phone."\nNachricht: \n".$message;
?>

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>GIMACON</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Creative Agency" />
	<link rel="icon" type="image/png" href="../images/favicon.png">
	<meta name="keywords" content="web agency, web design, web development, html5, css3, mobile first, responsive" />
	<meta name="google-site-verification" content="kA-nD0oOs83_uA0GyyAnaYxeXNRlIVqj-Fa9jXWWzoY" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="../favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="../css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="../css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="../css/simple-line-icons.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="../css/magnific-popup.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="../css/bootstrap.css">

	<link rel="stylesheet" href="../css/style.css">



	<style>
	h1 {
		color: white;
		margin-bottom: 5px;
		margin-top: 30px;
	}
	body {
		background-color: #282d2c;
	}

	.message {
		width: 100%;
		text-align: center;
		margin-top: 10%;
	}

	</style>


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<?php
		if (mail($to, $subject, $body, $headers)) {
			echo("<div class='message'><img src='../images/logo.png'><br><h1>Ihre Email wurde versendet!</h1><p>Sie werden in kürze zurückgeleitet.</p></div>");
		} else {
			echo("<div class='message'><img src='../images/logo.png'><br><h1>Es ist ein Fehler aufgetreten. Bitte versuchen Sie es später nochmals.</h1><p>Sie werden in kürze zurückgeleitet.</p></div>");
		}
	?>
	</body>
</html>
<script>
	setInterval(function(){ window.location.replace("http://gimacon.ch"); }, 3000);
</script>