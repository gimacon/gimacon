<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Gimacon | Allgemeine Geschäftsbedingungen</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Webagentur" />
	<link rel="icon" type="image/png" href="images/favicon.png">
	<meta name="keywords" content="webagentur, webdesign, web design, web development, webentwicklung, html5, css3, mobile first, responsive" />
	<meta name="google-site-verification" content="kA-nD0oOs83_uA0GyyAnaYxeXNRlIVqj-Fa9jXWWzoY" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="css/simple-line-icons.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/style.css">

	<!-- Styleswitcher ( This style is for demo purposes only, you may delete this anytime. ) -->
	<link rel="stylesheet" id="theme-switch" href="css/style.css">
	<!-- End demo purposes only -->

	<script src="js/modernizr-2.6.2.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>

	<header role="banner" id="fh5co-header">
			<div class="container">
				<!-- <div class="row"> -->
			    <nav class="navbar navbar-default">
		        <div class="navbar-header" style="margin-top: 9px;"">
		        	<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		        <img src="images/logo.png" id="logo"> 
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
		            <li><a href="index.php" class="external"><span>Zurück</span></a></li>
		          </ul>
		        </div>
			    </nav>
			  <!-- </div> -->
		  </div>
	</header>

	<section id="fh5co-home" data-section="home" style="background-image: url(images/full_image_2.jpg);" data-stellar-background-ratio="0.5">
		<div class="gradient"></div>
		<div class="container">
			<div class="text-wrap">
				<div class="text-inner">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1 class="to-animate">Allgemeine Geschäftsbedingungen</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slant"></div>
	</section>

	<div style="width: 80%; margin-left: 10%; margin-top: 5%;">
	<p><strong>1. Geltungsbereich</strong>
	<br><br>
	1.1 Die nachstehenden allgemeinen Geschäftsbedingungen gelten für den gesamten Geschäftsbereich des Gimacon, nachstehend “Gimacon” genannt, mit seinem Vertragspartner, nachstehend “Kunde” genannt.
	<br><br>
	1.2 Änderungen dieser Geschäftsbedingungen werden dem Kunden schriftlich bekannt gegeben. Sie gelten als genehmigt, wenn der Kunde nicht schriftlich Widerspruch erhebt. Der Kunde muss den Widerspruch innerhalb von vier Wochen nach Bekanntgabe der Änderungen an Gimacon absenden.
	<br><br>
	<strong>2. Weitergeleitete Aufträge</strong>
	<br><br>
	2.1 Wenn ein Auftrag in der Form ausgeführt wird, dass Gimacon einen Dritten mit der weiteren Erledigung betraut, erfüllt der Gimacon den Auftrag dadurch, dass er ihn im eigenen Namen an den Dritten weiterleitet.
	<br><br>
	2.2 Die Angebote des Gimacons sind frei bleibend und unverbindlich. Gültig sind die im individuellen Angebot genannten Preise.
	<br><br>
	<strong>3. Zustandekommen des Vertrages</strong>
	<br><br>
	3.1 Ein Vertrag mit Gimacon kommt in der Regel durch Zusendung der unterschriebenen Auftragsbestätigung durch den Kunden an Gimacon zustande.
	<br><br>
	3.2 Der Gegenstand des Vertrages ist folgender:
	Gimacon bietet Dienstleistungen und Produkte für den massgeschneiderten Internetauftritt des Kunden an. Die Dienstleistungen umfassen unter anderem die Erstellung von Websites, ihre Anpassung und Überarbeitung, Schulungs- und Beratungsdienstleistungen und Hosting-Dienstleistungen.
	Die vorliegenden allgemeinen Geschäftsbedingungen bilden als Anhang einen integrierenden Bestandteil des zwischen Gimacon und dem Kunden geschlossenen Arbeitsvertrags. Sie regeln die gegenseitigen Rechte und Pflichten der Parteien im Zusammenhang mit dem Abschluss, dem Inhalt und der Abwicklung des Vertrags.
	<br><br>
	<strong>4. Daten & Inhalte</strong>
	<br><br>
	Der Kunde stellt Gimacon von sämtlichen Ansprüchen Dritter hinsichtlich der überlassenen Daten frei. Im Falle eines Datenverlustes kann Gimacon nicht haftbar gemacht werden, sofern dieser nicht vorsätzlich oder grob fahrlässig handelt. Der Kunde verpflichtet sich, bei Nichtverschulden bzw. Vertretenmüssen des Gimacon, alle erforderlichen Daten erneut unentgeltlich an diesen zu übermitteln.
	<br><br>
	Der Kunde ist, sofern die Erstellung von Inhalten nicht explizit im Umfang des Auftrags enthalten ist, für die Bereitstellung von Inhalten (Texte, Grafiken, Videos etc.) der Produkte verantwortlich und stellt diese Gimacon so früh wie möglich in vollständiger, übersichtlicher und brauchbarer Form zur Verfügung. Gimacon haftet nicht für Missstände, welche durch fehlerhaftes, unvollständiges, ungenaues und/oder fehlendes Material entstehen.
	<br><br>
	Bei Fehlern, welche durch das Selbstverschulden des Kunden entstehen und nachgebessert werden müssen, behält sich Gimacon vor, die Priorität der Korrekturen an die eigenen Zeitpläne und Auslastung anzupassen und gegebenenfalls den dadurch entstandenen Mehraufwand dem Kunden zu verrechnen.
	<br><br>
	<strong>5. Datenschutz</strong>
	<br><br>
	Der Vertragspartner erklärt sich damit einverstanden, dass im Rahmen des mit ihm abgeschlossenen Vertrages, Daten über seine Person gespeichert, geändert und oder gelöscht und im Rahmen der Notwendigkeit an Dritte übermittelt werden. Dies gilt insbesondere für die Übermittlung von Daten, die für die Anmeldung und oder Änderung einer Domain (Internetadresse) notwendig sind.
	<br><br>
	<strong>6. Termine</strong>
	<br><br>
	Termine sind grundsätzlich erstreckbar. Sie sind nur verbindlich, wenn dies im Vertrag ausdrücklich vereinbart und so gekennzeichnet wird. Falls eine Partei erkennt, dass ein vereinbarter Termin nicht eingehalten werden kann, teilt sie dies der anderen Partei möglichst frühzeitig schriftlich mit. Termine, welche ausdrücklich als verbindlich bezeichnet wurden, können nur mit Zustimmung beider Parteien verschoben werden. Die Zustimmung darf nur in begründeten Fällen verweigert werden. Terminverzögerungen, welche durch die verspätete Mitwirkung des Kunden verursacht werden, können die Projektdauer verlängern.
	<br><br>
	<strong>7. Markenrechte/Copyrights</strong>
	<br><br>
	Der Kunde ist verpflichtet, alle rechtliche Verantwortung zu übernehmen, im Hinblick auf Urheberschutz, Jugendschutz, Presserecht und das “Recht am eigenen Bild”. Für vom Kunden beauftragte Veröffentlichungen sind nur Texte und Bilder zu veröffentlichen bzw. zur Veröffentlichung zur Verfügung zu stellen, an denen ein entsprechendes Nutzungsrecht besteht und zu denen das ggf. erforderliche Einverständnis abgebildeter Personen vorliegt. Das Urheberrecht auf alle durch Gimacon erstellten Arbeiten verbleibt bei der Gimacon.
	<br><br>
	<strong>8. Hosting</strong>
	<br><br>
	Diese Arbeiten werden nach bestem Wissen und Gewissen resp. in Verantwortung gegenüber dem Kunden erledigt. Die detaillierten Arbeiten und die jeweiligen Angebote für das Hosting werden in der Offerte festgelegt.
	<br><br>
	<strong>9. Preise und Zahlungen</strong>
	<br><br>
	Die Rechnungsstellung erfolgt nach Abschluss der in der Auftragsbestätigung festgelegten Arbeiten gemäss des individuell mit dem Kunden vereinbarten Preises. Alle Rechnungen sind in der Regel nach 30 Tagen und ohne Abzug zahlbar. 
	Andere Zahlungsmodalitäten werden in der Offerte und der Auftragsbestätigung festgesetzt; bei Erstkunden wird in der Regel mit der Auftragsbestätigung 1/3 des vereinbarten Rechnungsbetrages fällig. Der Rest 30 Tage nach Abschluss. Ausnahmen werden anhand einer Bonitätsprüfung entschieden und festgelegt.
	Ist der Kunde mit fälligen Zahlungen im Verzug, behält sich Gimacon vor, weitere Leistungen bis zum Ausgleich des offenen Betrages nicht auszuführen und hieraus entstandene Kosten an den Kunden weiterzugeben. Bei einer verspäteten Zahlung ist Gimacon berechtigt, nach vorgängiger Mahnung und dem erfolglosen Verstreichen einer Nachfrist, einen Verzugszins in der Höhe von 5% p.a. ab Fälligkeit (30 Tage nach Rechnungsstellung) zu berechnen.
	Befindet sich der Kunde mit der Bezahlung im Verzug, hat Gimacon das Recht, nach Ansetzung einer Nachfrist von 30 Tagen ohne weiteres vom Vertrag zurückzutreten. Von der Ausübung des Rücktrittsrechts wird der Kunde sofort in Kenntnis gesetzt. Wenn innert 45 Tagen nach Rechnungsstellung keine Zahlung eingegangen und keine Kündigung erfolgt ist, wird der Service, inkl. aller damit verbundenen Services (z.B. Websites) gesperrt. Für die entstandenen Umtriebe im Falle einer Sperrung erhebt Gimacon eine Bearbeitungsgebühr von CHF 60.– exkl. MwSt. Die Services werden erst nach Eingang der Zahlung inkl. Bearbeitungsgebühr wieder freigegeben. 
	Einwendungen wegen Unrichtigkeit oder Unvollständigkeit einer Rechnungsstellung hat der Kunde spätestens innerhalb von vier Wochen nach dessen Zugang zu erheben. Das Unterlassen rechtzeitiger Einwendungen gilt als Genehmigung.
	<br><br>
	<strong>10. Abnahme</strong>
	<br><br>
	Der Kunde ist verpflichtet, die Arbeitsergebnisse, welche ihm durch Gimacon während des Projekts zugestellt werden, zu prüfen und allfällige Mängel jeweils unverzüglich – aber spätestens zwei Monate nach Erteilung der mündlichen “Gut zur Online-Schaltung” – mitzuteilen.
	Mit der Erklärung “Gut zur Online-Schaltung” gilt die Leistung des Gimacon als abgenommen. Der Kunde darf die Erklärungen “Gut zur Online-Schaltung” nur bei Vorliegen schwerwiegender Mängel verweigern. In diesem Fall müssen die Mängel innerhalb einer angemessenen Frist behoben und das Arbeitsergebnis erneut zur Abnahme bereitgestellt werden.
	Können die Mängel auch nach der Ansetzung einer zweiten Nachfrist nicht behoben werden, dann stehen dem Kunden die gesetzlichen Rechte zu.
	Bestehen bei der Prüfung des Arbeitsergebnisses vor der Erklärung “Gut zur Online-Schaltung” nur noch geringfügige Mängel, dann gilt die Leistung auch ohne ausdrückliche Erklärung des Kunden als abgenommen. Die Mängel werden protokolliert und im Rahmen der Gewährleistungspflicht des Gimacon innerhalb einer angemessenen Frist kostenlos behoben.
	<br><br>
	<strong>11. Haftung</strong>
	<br><br>
	11.1 Das Risiko der rechtlichen Zulässigkeit der Tätigkeit und Erstellung von Projekten durch den Gimacon wird von dem Kunden getragen. Der Kunde stellt den Gimacon von Ansprüchen Dritter frei, wenn dieser auf ausdrücklichen Wunsch des Kunden gehandelt hat, obwohl er dem Kunden seine Bedenken im Hinblick auf die Zulässigkeit der Massnahmen mitgeteilt hat.
	<br><br>
	11.2 Erachtet Gimacon für die durchzuführenden Massnahmen eine wettbewerbsrechtliche Prüfung durch eine besonders sachkundige Person oder Institution für erforderlich, so trägt der Kunde nach Abstimmung die Kosten.
	<br><br>
	11.3 Schadensersatzansprüche gegen Gimacon sind ausgeschlossen, sofern sie nicht auf vorsätzlichem oder grob fahrlässigem Verhalten des Designers selbst oder dessen Erfüllungsgehilfen beruhen. Die Verjährungsfrist für die Geltendmachung von Schadensersatz beträgt drei Jahre und beginnt mit dem Zeitpunkt, an dem die Schadensersatzverpflichtung auslösende Handlung begangen worden ist. Sollten die gesetzlichen Verjährungsfristen im Einzelfall für Gimacon zu einer kürzeren Verjährung führen, gelten diese.
	<br><br>
	11.4 Für alle weiteren Schadensersatzansprüche gelten die gesetzlichen Bestimmungen.
	<br><br>
	11.5 Der Höhe nach ist die Haftung des Gimacon beschränkt auf die bei vergleichbaren Geschäften dieser Art typischen Schäden, die bei Vertragsschluss oder spätestens bei Begehung der Pflichtverletzung vorhersehbar waren.
	<br><br>
	11.6 Die Haftung des Gimacon für Mangelfolgeschäden aus dem Rechtsgrund der positiven Vertragsverletzung ist ausgeschlossen, wenn und soweit sich die Haftung desselben nicht aus einer Verletzung der für die Erfüllung des Vertragszweckes wesentlichen Pflichten ergibt.
	<br><br>
	<strong>12. Massgebliches Recht und Gerichtsstand</strong>
	<br><br>
	12.1 Für die Geschäftsverbindung zwischen dem Kunden und Gimacon gilt schweizerisches Recht.
	<br><br>
	12.2 Die Gerichtsstandvereinbarung gilt für Inlandskunden und Auslandskunden gleichermassen.
	<br><br>
	12.3 Erfüllungsort und Gerichtsstand für alle Leistungen und Auseinandersetzungen ist ausschliesslich der Geschäftssitz und das sachlich zuständige Gericht des Gimacon.
	<br><br>
	<strong>13. Sonstige Bestimmungen</strong>
	<br><br>
	13.1 Nebenabreden zu diesem Vertrag bestehen nicht. Änderungen oder Ergänzungen bedürfen zu ihrer Rechtswirksamkeit der Schriftform.
	<br><br>
	13.2 Der Kunde ist nicht berechtigt, seine Ansprüche aus dem Vertrag abzutreten.
	<br><br>
	<strong>14. Salvatorische Klausel</strong>
	<br><br>
	Sollte eine oder mehrere der vorstehenden Bestimmungen ungültig sein, so soll die Wirksamkeit der übrigen Bestimmungen hiervon nicht berührt werden. Dies gilt auch, wenn innerhalb einer Regelung ein Teil unwirksam, ein anderer Teil aber wirksam ist. Die jeweils unwirksame Bestimmung soll von den Parteien durch eine Regelung ersetzt werden, die den wirtschaftlichen Interessen der Vertragsparteien am nächsten kommt und die den übrigen vertraglichen Vereinbarungen nicht zuwider läuft.
	<br><br>
	<strong>Winterthur, Oktober 2017</strong><p>
	</p>
	</p>
	</div>
	<footer id="footer" role="contentinfo">
		<a href="#" class="gotop js-gotop"><i class="icon-arrow-up2"></i></a>
		<div class="container">
			<div class="">
				<div class="col-md-12 text-center">
					<p>&copy; Gimacon All Rights Reserved
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<ul class="social social-circle">
						<li><a target="_blank" href="https://www.facebook.com/gimacon.ch/"><i class="icon-facebook"></i></a></li>
						<li><a target="_blank" href="https://www.xing.com/xbp/pages/gimacon"><i class="icon-xing"></i></a></li>
						<li><a target="_blank" href="https://www.linkedin.com/company/27024027/"><i class="icon-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Counter -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="js/google_map.js"></script>
	<!-- Main JS (Do not remove) -->
	<script src="js/main.js"></script>
	</body>
	</html>