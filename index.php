
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Gimacon | Dein professioneller Auftritt optimal ins Netz gesetzt</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Webagentur" />
	<link rel="icon" type="image/png" href="images/favicon.png">
	<meta name="keywords" content="webagentur, webdesign, web design, web development, webentwicklung, html5, css3, mobile first, responsive" />
	<meta name="google-site-verification" content="kA-nD0oOs83_uA0GyyAnaYxeXNRlIVqj-Fa9jXWWzoY" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="Gimacon">
	<meta property="og:image" content="http://gimacon.ch/images/facebook_info.jpg">
	<meta property="og:url" content="http://www.gimacon.ch">
	<meta property="og:site_name" content="Gimacon">
	<meta property="og:description" content="Gimacon hat sich auf die Entwicklung von Marketing-Projekten spezialisiert. Zu unserem Team gehören Berater, Designer, Entwickler und Web-Nerds. Wir alle arbeiten mit Freude an Deiner Idee.">
	<meta name="twitter:title" content="Gimacon" >
	<meta name="twitter:image" content="http://gimacon.ch/images/facebook_info.jpg" >
	<meta name="twitter:url" content="http://www.gimacon.ch" >
	<meta name="twitter:card" content="Gimacon hat sich auf die Entwicklung von Marketing-Projekten spezialisiert. Zu unserem Team gehören Berater, Designer, Entwickler und Web-Nerds. Wir alle arbeiten mit Freude an Deiner Idee." >

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="css/simple-line-icons.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<link rel="stylesheet" href="css/style.css">

	<script src="js/modernizr-2.6.2.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<header role="banner" id="fh5co-header">
			<div class="container">
				<!-- <div class="row"> -->
			    <nav class="navbar navbar-default">
		        <div class="navbar-header" style="margin-top: 9px;"">
		        	<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		        <img src="images/logo.png" id="logo"> 
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
		            <li class="active"><a href="#" data-nav-section="home"><span>Home</span></a></li>
		            <li><a href="#" data-nav-section="work"><span>Portfolio</span></a></li>
		            <li><a href="#" data-nav-section="services"><span>Dienstleistungen</span></a></li>
		            <li><a href="#" data-nav-section="about"><span>Über Uns</span></a></li>
		            <li><a href="#" data-nav-section="contact"><span>Kontakt</span></a></li>
		            <li><a href="agb.php" class="external"><span>AGB</span></a></li>
		          </ul>
		        </div>
			    </nav>
			  <!-- </div> -->
		  </div>
	</header>

	<section id="fh5co-home" data-section="home" style="background-image: url(images/full_image_2.jpg);" data-stellar-background-ratio="0.5">
		<div class="gradient"></div>
		<div class="container">
			<div class="text-wrap">
				<div class="text-inner">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1 class="to-animate">Dein professioneller Auftritt optimal ins Netz gesetzt</h1>
							<h2 class="to-animate">Webseite | Logos | Apps | Grafiken | Marketingmaterial</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slant"></div>
	</section>

	<section id="fh5co-intro">
		<div class="container">
			<div class="row row-bottom-padded-lg">
				<div class="fh5co-block to-animate" style="background-image: url(images/img_7.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="fh5co-text">
						<i class="fh5co-intro-icon icon-bulb"></i>
						<h2>Planen</h2>
						<p>Wie willst Du Dich und dein Unternehmen der Welt präsentieren? Zusammen erarbeiten wir ein Projekt, welches Deinen Vorstellungen perfekt entspricht.</p><br><br>
						<p><a href="#fh5co-services" class="btn btn-primary">DIENSTLEISTUNGEN</a></p>
					</div>
				</div>
				<div class="fh5co-block to-animate" style="background-image: url(images/img_8.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="fh5co-text">
						<i class="fh5co-intro-icon icon-wrench"></i>
						<h2>Umsetzen</h2>
						<p>Ganz nach dem Motto „Made in Switzerland“ steht Qualität für uns an oberster Stelle. Mit Gimacon wird Dein Produkt optimal ins Licht gesetzt.</p><br><br>
						<p><a href="#fh5co-work" class="btn btn-primary">PORTFOLIO</a></p>
					</div>
				</div>
				<div class="fh5co-block to-animate" style="background-image: url(images/img_10.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="fh5co-text">
						<i class="fh5co-intro-icon icon-rocket"></i>
						<h2>Lancieren</h2>
						<p>Dein Projekt wird Realität: Dein Unternehmen verfügt nun über einen eleganten Web-Auftritt. Dieser soll jedoch auch in Zukunft noch zeitgemäss bleiben. Deshalb stehen wir Dir für kleinere und grössere Makeovers jederzeit tatkräftig zur Seite.</p>
						<p><a href="#fh5co-contact" class="btn btn-primary">KONTAKT</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="fh5co-work" data-section="work">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Portfolio</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>Taten sprechen lauter als Worte. Überzeuge Dich am besten selbst.</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-bottom-padded-sm">
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0007/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_7.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Travel Agency</h2>
						<span>Web, Design</span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0008/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_8.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Fotostudio Landing Page</h2>
						<span>Branding, Web, Design</span>
						</div>
					</a>
				</div>

				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0009/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_9.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>App Landing Page</h2>
						<span>Web, Design</span>
						</div>
					</a>
				</div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0001/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_1.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Hochzeitsfotograf</h2>
						<span>Web</span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0002/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_2.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Restaurant</h2>
						<span>Web</span>
						</div>
					</a>
				</div>

				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0003/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_3.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Fahrradladen</h2>
						<span>Web, Design</span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0004/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_4.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>App Website</h2>
						<span>Web, Design</span>
						</div>
					</a>
				</div>
				
				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0005/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_5.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Admin Screen</h2>
						<span>Web, Development, Design</span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12">
					<a href="references/0006/index.html" target="_blank" class="fh5co-project-item to-animate">
						<img src="images/work_6.jpg" alt="Image" class="img-responsive">
						<div class="fh5co-text">
						<h2>Fotografie Agentur</h2>
						<span>Web, Design</span>
						</div>
					</a>
				</div>
				
				<div class="clearfix visible-sm-block"></div>
			</div>
		</div>
	</section>

	<section id="fh5co-services" data-section="services">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-left">
					<h2 class=" left-border to-animate">Dienstleistungen</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 fh5co-service to-animate">
					<i class="icon to-animate-2 icon-anchor"></i>
					<h3>Brand &amp; Strategy</h3>
					<p>Wir helfen Dir aus den Startlöchern oder frischen Deinen Look auf: Online-Marketing; Logos, Briefschaften, Visitenkarten, Werbemittel und vieles mehr. </p>
				</div>
				<div class="col-md-6 col-sm-6 fh5co-service to-animate">
					<i class="icon to-animate-2 icon-layers2"></i>
					<h3>Web &amp; Software</h3>
					<p>Wir erstellen für Dich eine Webseite mit modernster Benutzerführung und einer ansprechenden Gestaltung – massgeschneidert nach Deinen Vorstellungen. Gimacon kennt die Trends und scheut auch keine neuen Wege. Deine Website soll schliesslich ganz oben Erscheinen.</p>
				</div>

				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-6 col-sm-6 fh5co-service to-animate">
					<i class="icon to-animate-2 icon-video2"></i>
					<h3>Photo &amp; Video</h3>
					<p>Professionelle Bilder- und Videobearbeitung, Zeichnungen, Animationen, Retouchierung. Wir inspirieren Dich zu einem einladenden und aufregenden Webauftritt.</p>
				</div>
				<div class="col-md-6 col-sm-6 fh5co-service to-animate">
					<i class="icon to-animate-2 icon-monitor"></i>
					<h3>CMS &amp; eCommerce</h3>
					<p>Deine Homepage kannst du dank kinderleichtem und inituitivem Handling jederzeit selber ergänzen. Auch die Verwaltung deines Webshops benötigt keinerlei Vorkenntnisse.</p>
				</div>
				
			</div>
		</div>
	</section>
	
	<section id="fh5co-about" data-section="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Über Uns</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>Gimacon hat  sich auf die Entwicklung von Marketing-Projekten spezialisiert. Zu unserem Team gehören Berater, Designer, Entwickler und Web-Nerds. Wir alle arbeiten mit Freude an Deiner Idee.</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="fh5co-person text-center to-animate">
						<figure><img src="images/person1.jpg" alt="Image"></figure>
						<h3>Roberto Fulgieri</h3>
						<span class="fh5co-position">Sales & Accounting</span>
						<p>Ist um kein Wort verlegen und hat ein Flair für Zahlen. Versteckte Kosten sind sein grösster Gegner.</p><br>
						<ul class="social social-circle">
							<li><a target="_blank" href="mailto:fulgieri@gimacon.ch"><i class="icon-envelope"></i></a></li>
							<li><a target="_blank" href="https://www.linkedin.com/in/roberto-fulgieri-0b80a314a/"><i class="icon-linkedin"></i></a></li>
							<li><a target="_blank" href="https://www.xing.com/profile/Roberto_Fulgieri?sc_o=da980_e"><i class="icon-xing"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="fh5co-person text-center to-animate">
						<figure><img src="images/person2.jpg" alt="Image"></figure>
						<h3>Leonard Manser</h3>
						<span class="fh5co-position">Support & Customer Relations</span>
						<p>Zuständig für die Konzeptentwicklung, hat stets ein offenes Ohr für Deine Pläne. Hält Dich immer auf dem laufenden.</p>
						<ul class="social social-circle">
							<li><a target="_blank" href="mailto:manser@gimacon.ch"><i class="icon-envelope"></i></a></li>
							<li><a target="_blank" href="https://www.linkedin.com/in/leonard-manser-204a4a151/"><i class="icon-linkedin"></i></a></li>
							<li><a target="_blank" href="https://www.xing.com/profile/Leonard_Manser2?sc_o=da980_e"><i class="icon-xing"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="fh5co-person text-center to-animate">
						<figure><img src="images/person3.jpg" alt="Image"></figure>
						<h3>Garan Sonsat</h3>
						<span class="fh5co-position">Web Director</span>
						<p>Code Monkey mit Affinität für Design und Technik. Zuständig für Projektleitung. Fühlt sich vor dem Computer am wohlsten.</p>
						<ul class="social social-circle">
							<li><a target="_blank" href="mailto:sonsat@gimacon.ch"><i class="icon-envelope"></i></a></li>
							<li><a target="_blank" href="https://www.linkedin.com/in/garan-sonsat-9101a9b2/"><i class="icon-linkedin"></i></a></li>
							<li><a target="_blank" href="https://www.xing.com/profile/Garan_Sonsat?sc_o=mxb_p"><i class="icon-xing"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section id="fh5co-counters" style="background-image: url(images/full_image_2.jpg);" data-stellar-background-ratio="0.5">
		<div class="fh5co-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center to-animate">
					<h2>Fun Facts</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="fh5co-counter to-animate">
						<i class="fh5co-counter-icon icon-briefcase to-animate-2"></i>
						<span class="fh5co-counter-number js-counter" data-from="0" data-to="38" data-speed="1500" data-refresh-interval="50">0</span>
						<span class="fh5co-counter-label">Abgeschlossene Projekte</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="fh5co-counter to-animate">
						<i class="fh5co-counter-icon icon-code to-animate-2"></i>
						<span class="fh5co-counter-number js-counter" data-from="0" data-to="934568" data-speed="1500" data-refresh-interval="50">934012</span>
						<span class="fh5co-counter-label">Codezeilen</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="fh5co-counter to-animate">
						<i class="fh5co-counter-icon icon-cup to-animate-2"></i>
						<span class="fh5co-counter-number js-counter" data-from="0" data-to="341" data-speed="1500" data-refresh-interval="50">290</span>
						<span class="fh5co-counter-label">Tassen Kaffee</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="fh5co-counter to-animate">
						<i class="fh5co-counter-icon icon-people to-animate-2"></i>
						<span class="fh5co-counter-number js-counter" data-from="0" data-to="33" data-speed="1500" data-refresh-interval="50">0</span>
						<span class="fh5co-counter-label">Zufriedene Kunden</span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="fh5co-contact" data-section="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Kontaktieren Sie uns!</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
						</div>
					</div>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				

				<div class="col-md-6 to-animate">
					<section id="contact">
        <div class="container">
            <div class="row">
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form id="contactForm" action="mail/contact_me.php" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control" placeholder="Name *" id="name" required data-validation-required-message="Bitte geben sie Ihren Namen ein.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control" placeholder="Email *" id="email" required data-validation-required-message="Bitte geben Sie Ihre Email ein.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input name="phone" type="tel" class="form-control" placeholder="Telefon" id="telephone">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" placeholder="Ihre Nachricht *" id="message" required data-validation-required-message="Bitte geben Sie eine Nachricht ein."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                            	<div class="g-recaptcha" data-sitekey="6LdrmTQUAAAAABHEMz1Pr9JV-MXw102sGXVv9xWD"></div>
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">SENDEN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="adress"><p>Gimacon | c/o Roberto Fulgieri | Hörnlistrasse 80 | 8400 Winterthur | info@gimacon.ch</p></div>
        </div>
    </section>
				</div>

			</div>
		</div>
	</section>
	
	
	<footer id="footer" role="contentinfo">
		<a href="#" class="gotop js-gotop"><i class="icon-arrow-up2"></i></a>
		<div class="container">
			<div class="">
				<div class="col-md-12 text-center">
					<p>&copy; Gimacon All Rights Reserved
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<ul class="social social-circle">
						<li><a target="_blank" href="https://www.facebook.com/gimacon.ch/"><i class="icon-facebook"></i></a></li>
						<li><a target="_blank" href="https://www.xing.com/xbp/pages/gimacon"><i class="icon-xing"></i></a></li>
						<li><a target="_blank" href="https://www.linkedin.com/company/27024027/"><i class="icon-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Counter -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="js/google_map.js"></script>
	<!-- Main JS (Do not remove) -->
	<script src="js/main.js"></script>

	</body>


